package az.ingress.market.service;

import az.ingress.market.dto.ManagerDto;
import java.util.List;

public interface ManagerService {
    ManagerDto createManager(ManagerDto managerDto);

    ManagerDto updateManager(Integer managerId, ManagerDto managerDto);


    List<ManagerDto> findAll();

    ManagerDto findById(Integer id);
    void deleteManager(Integer id);


}
