package az.ingress.market.mapper;

import az.ingress.market.dto.ManagerDto;
import az.ingress.market.model.Manager;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ManagerMapper {
    ManagerDto mapToManagerDto(Manager manager);
    Manager mapToManager(ManagerDto managerDto);
    List<ManagerDto> mapToManagerDtoList(List<Manager> taskList);
}
